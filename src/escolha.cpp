#include "escolha.hpp"
#include "categoria.hpp"
#include <iostream>
#include <string>
#include "cliente.hpp"
#include "socio.hpp"
#include "fstream"


using namespace std;

template <typename T1>
T1 getInput()
{
	while (true)
	{
		T1 valor;
		cin >> valor;
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(32767, '\n');
			cout << "Entrada inválida! Insira novamente: " << endl;
		}
		else
		{
			cin.ignore(32767, '\n');
			return valor;
		}
	}
}

string getString(){
    while(true){
        string valor;
        try{
            getline(cin, valor);    
        }
        catch(const std::ios_base::failure& e){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return valor;
    }
}


void estoque(){
    int r;
    fstream arquivo;

    while(true){
        cout<<"*******************************************"<<endl;
        cout<<"| 1 -        Adicionar categoria game     |"<<endl;
        cout<<"*******************************************"<<endl;
        cout<<"| 2 -          Adicionar game             |"<<endl;
        cout<<"*******************************************"<<endl;
        cout<<"| 3 -        Visualizar o estoque         |"<<endl;
        cout<<"*******************************************"<<endl;
        cout<<"| 4 -                 sair                |"<<endl;
        cout<<"*******************************************"<<endl;
            
        cout<<r;
    
            switch (r)
            {
                case 1:{
                    
                
                    string Nome_game;

                    cout << "Nome do tipo de jogo: ";
                    Nome_game = getString();

                    arquivo.open("doc/Estoque/Categorias.txt", ios::in);
                    if (arquivo.is_open())
                    {
                        string linha;

                        while (getline(arquivo, linha))
                        {
                            if (linha == Nome_game)
                            {
                                r++;
                                arquivo.close();
                            }
                        }
                        arquivo.close();
                    }

                    if (r != 0)
                    {
                        cout << endl;
                        cout << "Tipo já registrado!" << endl;
                        cout<< endl;
                    }

                    else
                    {
                        arquivo.open("doc/Estoque/Categorias.txt", ios::out | ios::app);

                        cat cat(Nome_game);

                        arquivo << cat.get_cat() << endl;

                        cout << endl;

                        arquivo.close();
                    }

                    break;
                
                }
                case 2:
                    //estoque_loja.adiciona_nova_categoria();
                    break;

                case 4:
                    //estoque_loja.adiciona_produto();
                    break;
                case 0:
                    exit(0);
                    break;
                default:
                    cout<<"ERRO - Digite uma opção registrada de 1 a 4"<<endl;
                    system("tput reset");
                    break;
            }


        }
        
        return ;
    
}
