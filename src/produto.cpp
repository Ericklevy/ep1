#include <string>
#include <iostream>
#include "produto.hpp"

using namespace std;

Jogo::Jogo(){
    set_nome("");
    set_valor(0.0f);
    set_total(0);
}

Jogo::Jogo(vector<cat> cats, string nome, float valor, int total){
    set_nome(nome);
    set_valor(valor);
    set_total(total);
    set_cat(cats);
    
}

Jogo::~Jogo(){
}

string Jogo::get_nome(){
    return nome;
}

void Jogo::set_nome(string nome){
    this->nome = nome;
}

float Jogo::get_valor(){
    return valor;
}

void Jogo::set_valor(float valor){
    this->valor = valor;
}

int Jogo::get_total(){
    return total;
}

void Jogo::set_total(int total){
    this->total = total;
}

vector <cat> Jogo::get_cat(){
    return cats;
}

void Jogo::set_cat(vector <cat> cats){
    this->cats = cats;
}



