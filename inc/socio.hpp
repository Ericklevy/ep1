#ifndef SOCIO_HPP
#define SOCIO_HPP


#include <iostream>
#include <string>
#include "cliente.hpp"

class Socio : public Cliente{
    private:
        int socio;
    public:
        Socio();
        Socio(string nome, string cpf, string email,  string tel, int idade, int socio);
        Socio(string nome);
        ~Socio();
        int get_socio();
        void set_socio(int socio);
    
        
        void imprime_dados();
    
};
#endif
