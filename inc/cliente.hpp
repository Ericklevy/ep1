#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include <cstring>
#include <string>



using namespace std;
//Classe do trabalho
class Cliente{
    //metodos
    public:
        Cliente();
        ~Cliente();
        string get_nome();
        void set_nome(string nome);
        
        string get_cpf();
        void set_cpf(string cpf);
        
        string get_email();
        void set_email(string email);
        
        string get_tel();
        void set_tel(string tel);
        
        int get_idade();
        void set_idade(int idade);
    //atributos
    private:
        string nome;
        string cpf;
        string email;
        string tel;
        int idade;
        
        
        virtual void imprime_dados() = 0;
};
#endif
