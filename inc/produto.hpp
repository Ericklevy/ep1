#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <iostream>
#include <vector>
#include "categoria.hpp"

using namespace std;

class Jogo{
    public:
        Jogo();
        Jogo(vector<cat> cats,string nome, float valor, int total);
        ~Jogo();
        
        vector<cat> get_cat();
        void set_cat(vector<cat> cats);
        
        string get_nome();
        void set_nome(string nome);
        
        float get_valor();
        void set_valor(float valor);
        
        int get_total();
        void set_total(int total);
        
    private:
        string nome;
        float valor;
        int total;
        vector <cat> cats;
        
    
};
#endif
